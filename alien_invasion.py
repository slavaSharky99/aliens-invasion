# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 23:57:48 2017

@author: slava
"""

import sys
import pygame 

from pygame.sprite import Group

from settings import Settings
from ship import Ship
#from alien import Alien

import game_function as gf

def run_game():
    
    my_settings = Settings()
    pygame.init()
    screen = pygame.display.set_mode((my_settings.screen_width, my_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")
    
    ship = Ship(screen, my_settings)
    bullets = Group()
    aliens = Group()
    
    gf.create_fleet(my_settings, screen, ship, aliens)
    while True:
        
        gf.check_events(my_settings, screen, ship, bullets)
        ship.update()

        gf.update_bullets(bullets)
        
        gf.update_screen(my_settings, screen, ship, aliens, bullets)
        

run_game()