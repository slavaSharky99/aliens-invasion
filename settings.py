# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 00:11:53 2017

@author: slava
"""

class Settings():
    
    def __init__(self):
        
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (50, 80, 240)
        self.ship_speed_factor = 1.5
        
        self.bullet_speed_factor = 1
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullt_allowed = 5